/*
1)Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
2)Які засоби оголошення функцій ви знаєте?
3)Що таке hoisting, як він працює для змінних та функцій?

1) Екранування це спосіб додавання спецсимволів до рядків, виконується за допомогою зворотнього слешу \
2)  - function declaration;
    - function expression(стрілочні функції відносяться сюди)
    - named function expression.
3) Hoisting - це механізм який піднімає змінні та оголошення функції в гору своєї видимості, що, наприклад, дає змогу 
   викликати функцію навіть до її оголошення в коді(на function expression це не розповсюджується )

*/

function createNewUser() {
    let firstName = prompt("Enter your first name")
    let secondName = prompt("Enter your second name")
    let birthday = prompt("Enter your birthday", "dd.mm.yyyy").split(".")


    let newUser = {
        firstName: firstName,
        secondName: secondName,
        birthday: birthday = new Date(birthday[2], +birthday[1] - 1, +birthday[0]),

        getLogin() {
            let shortName = (this.firstName[0] + this.secondName).toLowerCase()
            return shortName
        },

        getAge() {
            let todayDate = new Date()
            let todayDay = todayDate.getDay()
            let todayMonth = todayDate.getMonth()
            let todayYear = todayDate.getFullYear()
            let age = (todayYear - birthday.getFullYear())
            if ((todayMonth < birthday.getMonth()) || (todayDay < birthday.getDay())) {
                age -= 1
            }
            return `Your age - ${age}`
        },

        getPassword() {
            let birthdayYear = birthday.getFullYear()
            return this.firstName[0].toUpperCase() + this.secondName.toLowerCase() + birthdayYear
        }
    }

    Object.defineProperty(newUser, ['firstName', 'secondName'], {
        setFirstName(value) {
            this.firstName = value
        },
        setLastName(value) {
            this.secondName = value
        },
    })
    //console.log(birthday)

    return newUser
}

const user = createNewUser()
console.log(user.getLogin())
console.log(user.getAge())
console.log(user.getPassword())
